PicoRio Document
================

About
-----

This is a read-the-docs-like document producted by Sphinx
 
Sphinx installation
-------------------

To install Sphinx and dependencies:
```
  pip install -U  sphinx sphinx-autobuild sphinx_rtd_theme
```
The version of Sphinx shoule be >= 1.8.5

Build
-----

To build the project into HTML:
```
  cd picorio-doc
  make html
```

After build succeeded, the build outputs are in:
```
  picorio-doc/build/html
```

You can check results by open ```index.html``` in local browser.

Source File Structure
---------------------

```
.
├── make.bat
├── Makefile
├── README.md
└── source
    ├── conf.py
    ├── favicon.ico
    ├── general
    │   ├── FAQ.rst
    │   ├── introduction.rst
    │   └── roadmap.rst
    ├── general.rst
    ├── images
    ├── index.rst
    ├── software
    │   ├── v8
    │   │   ├── community_operation.rst
    │   │   ├── design
    │   │   ├── design.rst
    │   │   ├── for_developers
    │   │   ├── for_developers.rst
    │   │   ├── getstart
    │   │   ├── getstart.rst
    │   │   ├── project_management
    │   │   └── project_management.rst
    │   └── v8.rst
    └── software.rst

```
Source files are located in ```picorio-doc/source```.
 * By modifying ```conf.py``` you can change the HTML style.
 * By modifying ```index.rst``` you can change the home page and contents.
 * By modifying and adding other ```.rst``` files, 
   you can change and add new section in the document.